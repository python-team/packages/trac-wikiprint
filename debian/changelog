trac-wikiprint (4.0.0+svn18614+really3.0.0+svn18295-1) unstable; urgency=medium

  * Team upload
  * Downgrade version to 3.0.0+svn18295
  * Drop Depends: python3-pdfkit (Closes: #1093563)
  * Recover license info from older version
  * Recommend python3-pygments
  * Fix invalid escape sequences by using raw strings

 -- Bastian Germann <bage@debian.org>  Wed, 22 Jan 2025 19:25:47 +0100

trac-wikiprint (4.0.0+svn18614-2) unstable; urgency=medium

  * Team upload.
  * Port to importlib.util (closes: #1091357).

 -- Colin Watson <cjwatson@debian.org>  Mon, 20 Jan 2025 00:33:58 +0000

trac-wikiprint (4.0.0+svn18614-1) unstable; urgency=medium

  * New upstream snapshot

 -- Martin <debacle@debian.org>  Sun, 21 Apr 2024 13:42:26 +0000

trac-wikiprint (4.0.0+svn18314-2) unstable; urgency=medium

  * Add patch to remove unsupported wkhtmltopdf options (WiP)
  * Adapt debian/clean to new layout

 -- Martin <debacle@debian.org>  Sun, 22 Oct 2023 14:01:50 +0000

trac-wikiprint (4.0.0+svn18314-1) unstable; urgency=medium

  * New upstream version, compatible with Trac 1.6 and Python 3
  * Complete rewrite, based on wkhtmltopdf instead of xhtml2pdf

  [ Ondřej Nový <onovy@debian.org> 2018-03-12 ]
  * d/copyright: Use https protocol in Format field
  * d/control: Remove trailing whitespaces
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Martin <debacle@debian.org>  Tue, 17 Oct 2023 08:59:32 +0000

trac-wikiprint (2.0.0+r16816-1) unstable; urgency=medium

  * New upstream version
  * Bump standards version and dh compat level
  * Depend on python-pil (Closes: #866488)
  * Depend on python-xhtml2pdf
  * Move git repo to salsa.debian.org

 -- W. Martin Borgert <debacle@debian.org>  Tue, 27 Feb 2018 21:45:18 +0000

trac-wikiprint (2.0.0+r15603-1) unstable; urgency=low

  * New upstream version
  * Move to Git packaging repo
  * Use debian/clean
  * Machine readable copyright
  * Bump standards-version to 3.9.8 (no changes)

 -- W. Martin Borgert <debacle@debian.org>  Sat, 30 Jul 2016 21:23:03 +0000

trac-wikiprint (1.9.2-2) unstable; urgency=low

  * Team upload.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Jackson Doak ]
  * Accept NMU changes
  * Add debian/watch, thanks bartm.
  * Don't use symlinks in d/copyright
  * Add dh_clean override to fix FTBFS if built twice in a row. Closes: #671334
    Thanks Javi Merino
  * Bump debhelper version to 9
  * Bump standards-version to 3.9.5 (no changes)

 -- Jackson Doak <noskcaj@ubuntu.com>  Sun, 23 Feb 2014 08:16:29 +1100

trac-wikiprint (1.9.2-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "depends on nonexisting python-pil if rebuilt"
    add a debian/pydist-overrides file.
    (Closes: #680423)

 -- gregor herrmann <gregoa@debian.org>  Wed, 01 Aug 2012 16:51:11 +0200

trac-wikiprint (1.9.2-1) unstable; urgency=low

  * New upstream version (Closes: #653728). Works with Trac 0.12.
  * Improve package description (Closes: #572567).
  * Fix lintian.

 -- W. Martin Borgert <debacle@debian.org>  Sat, 31 Dec 2011 21:18:01 +0000

trac-wikiprint (1.6+r5729-2) unstable; urgency=low

  * Added missing copyright information about original authors. Thanks
    to Torsten Werner for noticing - and accepting the package anyway!

 -- W. Martin Borgert <debacle@debian.org>  Sun, 21 Feb 2010 01:06:43 +0000

trac-wikiprint (1.6+r5729-1) unstable; urgency=low

  * First Debian package (Closes: #558317).

 -- W. Martin Borgert <debacle@debian.org>  Tue, 09 Feb 2010 13:29:30 +0000
